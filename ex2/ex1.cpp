#include <iostream>
#include <thread>
#include <mutex>
#include <time.h>

std::mutex out;

#define TIMER

static inline int64_t
time_elapsed (struct timespec a, struct timespec b)
{
	return ((int64_t)a.tv_sec - b.tv_sec) * 1000000
			+ ((int64_t)a.tv_nsec - b.tv_nsec) / 1000LL;
}

static inline struct timespec
time_gettime ()
{
	struct timespec s;
	clock_gettime(CLOCK_REALTIME, &s);
	return s;
}

static inline void
time_print_elapsed (const char *id, struct timespec start)
{
	struct timespec finish = time_gettime ();
	std::cout << "--- [" << id << "] time elapsed " << time_elapsed(finish, start) << " ns." << std::endl;
}

void calcArea(double delta, int from, int to, double* result)
{
  double x = ((double) from) * delta;
  double res = 0.0;

  while(from < to){
    double fx, fx1;
    fx = 4/(1+x*x);
    fx1 = 4/(1+(x+delta)*(x+delta));

    res += (fx+fx1/2)*delta;
    x += delta;
    from++;
  }

  out.lock();
  *result += res;
  out.unlock();


//   while (true)
//     {
//       out.lock();
//       std::cout << "Philosopher " << n << " is thinking." << std::endl;
//       out.unlock();

//       left->lock();
//       out.lock();
//       std::cout << "Philosopher " << n << " picked up her left fork." << std::endl;
//       out.unlock();

//       if(right->try_lock()){
//         out.lock();
//         std::cout << "Philosopher " << n << " picked up her right fork." << std::endl;
//         out.unlock();

//         out.lock();
//         std::cout << "Philosopher " << n << " is eating." << std::endl;
//         out.unlock();

//         out.lock();
//         std::cout << "Philosopher " << n << " is putting down her right fork." << std::endl;
//         out.unlock();
//         right->unlock();
//       }

//       out.lock();
//       std::cout << "Philosopher " << n << " is putting down her left fork." << std::endl;
//       out.unlock();
//       left->unlock();

//     }
}

void usage(char *program)
{
  std::cout << "Usage: " << program << " N  M (where 1<=N<=64 and 1<=M<=2147483647)" << std::endl;
  std::cout << "Alternatively use -h for more information." << std::endl;
  exit(1);
}

void help(char *program){
  std::cout << "Help for command \"" << program << "\"." << std::endl;
  std::cout << "Usage: " << program << " N  M (where 1<=N<=64 and 1<=M<=2147483647)" << std::endl;
  std::cout << "N denotes the amount of threads to use by the program." << std::endl;
  std::cout << "M denotes the amount of trapezoids that are used in the program." << std::endl;
  std::cout << "alternatively use \"" << program << " -h\" to display the help menu." << std::endl;
}

bool compare(char* input, const char* checksum, uint length){
  if(sizeof(input) == sizeof(checksum)){
    uint i = 0;
    while(i < length){
      if(input[i] != checksum[i]){
	return false;
      }
      i++;
    }
    return true;
  }
  return false;
}

int main(int argc, char *argv[])
{
  if(argc == 2){
    const char* helpflag = "--help";
    const char* hflag = "-h";
    if(compare(argv[1], hflag, 2) || compare(argv[1], helpflag, 6)){
      help(argv[0]);
    } else {
      usage(argv[0]);
    }
    return 0;
  }

  if (argc != 3)
    {
      usage(argv[0]);
      return 0;
    }

  // philosophers = argv[1]
  int i, cpus, fragments;
  double delta, res;
  try
    {
      cpus = std::stoi(argv[1]);
      fragments = std::stoi(argv[2]);
    }
  catch (std::exception)
    {
      usage(argv[0]);
    }
  if (cpus < 1 || cpus >= 64 || fragments < 1 || fragments >= 2147483647)
    {
      usage(argv[0]);
      return 0;
    }

  double threadspf = fragments / (double) cpus;

  int from = 0;
  int to = 0;
  res = 0.0;
  delta = 1.0 / (double) fragments;

  std::thread *ph = new std::thread[cpus];

#if defined(TIMER)
  struct timespec start;
  start = time_gettime ();
#endif

  for(i = 0; i < cpus; i++){
    from = (int) (threadspf * (double) i);
    to = (int) (threadspf * (double) (i+1));
//    out.lock();
//    std::cout << "fragment " << i << " is: \"" << from << "-" << to << "\"." << std::endl;
//    out.unlock();
    ph[i] = std::thread(calcArea, delta, from, to, &res);
  }
  for(i = 0; i < cpus; i++){
    ph[i].join();
  }

#if defined(TIMER)
  time_print_elapsed(__func__, start);
#endif

  out.lock();
  std::cout << "Result is: \"" << res << "\"." << std::endl;
  out.unlock();

  // forks
  // std::mutex *forks = new std::mutex[philosophers];

  // philosophers
  // std::thread *ph = new std::thread[philosophers];
  // for (int i=0; i<philosophers; ++i)
    // {
      // int left = i;
  //     int right = (i == 0 ? philosophers : i) - 1;
  //     ph[i] = std::thread(philosopher, i, &forks[left], &forks[right]);
  //   }

  // ph[0].join();
  // delete[] forks;
  delete[] ph;

  return 1;
}
